#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue, May 30th 2023
Written during WIN FMRIB/OHBA Analysis groups' PyPhyPred hackathon
#add names   
A separate copy of the licence is also included within the repository.

This file includes helper functions for reading from / writing to disk.
 
"""

###############################################################################

#import python packages that you need here; e.g. numpy scipy etc

###############################################################################

def load_file(filename, fileformat='', var_names='', subj_ids='', **kwargs):
    # This function takes a filename and loads it into a numpy array.
    # If the fileformat is not user-specified, it will be determined 
    # automatically based on filename extension.
    #
    # Supported file formats:       
    # .txt
    #    TXT file can only contain numeric values, no header row; variable 
    #    must be set with `var_names` argument, and subject IDs with 
    #    `subj_ids` argument.
    # .csv
    #    CSV files are expected to have a header row with variable names, 
    #    and first column must be subject IDs; header row can be omitted
    #    if `var_names` is given, and first column will be treated as any 
    #    other variable if `subj_ids` is given.
    # .mat 
    #    MAT file must represent a single 2D array, represented as either
    #     - MATLAB table object, with named columns giving variable names
    #       and named rows giving subject IDs.
    #     - MATLAB 2D array, a string array of column names and 
    #       string or numeric array of row names
    # .npy
    # .npz
    #
    # For all input formats, the convention is
    # rows - Different individuals
    # cols - Different variables
    # 
    return #output_array

def save_file(filename, fileformat='', **kwargs):
    #this function takes a filename and loads it into a numpy array
    #if the fileformat is not user-specified, determine it automatically based on filename extension
    #throw an error if file format is not supported
    #example formats that could be good to support: .npy, .npz, .txt, .csv, .mat 
    return #output_array